from django import forms
from .models import Parent, Child
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
class ParentForm(forms.ModelForm):
    class Meta:
        model = Parent
        fields = ['name', 'contact_number', 'address']

class ChildForm(forms.ModelForm):
    class Meta:
        model = Child
        fields = ['name', 'birth_date', 'group', 'parent']

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['parent'].queryset = Parent.objects.all()


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']



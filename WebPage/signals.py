from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.dispatch import receiver
from .models import UserSession
from django.utils import timezone

@receiver(user_logged_in)
def log_user_login(sender, user, request, **kwargs):
    group = user.groups.first()
    UserSession.objects.create(user=user, group=group)

@receiver(user_logged_out)
def log_user_logout(sender, user, request, **kwargs):
    user_session = UserSession.objects.filter(user=user, logout_time__isnull=True).last()
    if user_session:
        user_session.logout_time = timezone.now()
        user_session.duration = user_session.logout_time - user_session.login_time
        user_session.save()
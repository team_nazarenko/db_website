��    0      �  C         (     )  	   1  
   ;     F  =   N  ?   �     �  
   �     �     �     �                    +     9  
   >     I     U  b   [  
   �  
   �     �     �  	   �     �     �     
            L        l     q     y     �     �     �     �     �     �     �  	   �     �     �     �       
     �    	   �     �     �     	  ;   	  =   Q	     �	     �	     �	     �	     �	     �	     �	     �	     �	  	   
     
     &
     8
  r   @
     �
     �
     �
     �
     �
     �
     �
       	   	       X        t     {     �     �     �  	   �     �  
   �     �     �     �            &   2     Y     e                              '                              
                         &   $   /                    *      +      ,           0                 "      (         	         #   .          %      !      -                 )       Actions Add Child Add Parent Address Are you sure you want to delete this child: {{ child.name }}? Are you sure you want to delete this parent: {{ parent.name }}? Assign Role Birth Date Cancel Change Password Children List Contact Number Delete Delete Child Delete Parent Edit Edit Child Edit Parent Email Explore our platform to manage lists of children, their groups, parent information, and much more. First Name Form Title Group Home Last Name List of Children List of Parents Log In Log Out Login My website provides convenient and useful services for parents and children. Name Parents Placeholder Image Register Registration Save Submit Unauthorized Update Update User User List Username Website Image Welcome to My Website Yes, delete Your Image Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n % 10 == 1 && (n % 100 > 19 || n % 100 < 11) ? 0 : (n % 10 >= 2 && n % 10 <=9) && (n % 100 > 19 || n % 100 < 11) ? 1 : n % 1 != 0 ? 2: 3);
 Darbības Pievienot Bērnu Pievienot Vecāku Adrese Vai tiešām vēlaties dzēst šo bērnu: {{ child.name }}? Vai tiešām vēlaties dzēst šo vecāku: {{ parent.name }}? Piešķirt Lomu Dzimšanas Datums Atcelt Mainīt Paroli Bērnu Saraksts Kontakttelefons Dzēst Dzēst Bērnu Dzēst Vecāku Rediģēt Rediģēt Bērnu Rediģēt Vecāku E-pasts Izpētiet mūsu platformu, lai pārvaldītu bērnu sarakstus, viņu grupas, vecāku informāciju un daudz vairāk. Vārds Formas Nosaukums Grupa Mājas Uzvārds Vecāku Bērnu Vecāku Saraksts Ienākt Iziešana Ienākt Mana tīmekļa vietne piedāvā ērtas un noderīgas pakalpojumus vecākiem un bērniem. Vārds Vecāki Vietturis Attēls Reģistrēties Reģistrācija Saglabāt Iesniegt Neatļauts Atjaunināt Atjaunināt Lietotāju Lietotāju Saraksts Lietotājvārds Tīmekļa Vietnes Attēls Laipni Lūdzam Manā Tīmekļa Vietnē Jā, dzēst Jūsu Attēls 
��    0      �  C         (     )  	   1  
   ;     F  =   N  ?   �     �  
   �     �     �     �                    +     9  
   >     I     U  b   [  
   �  
   �     �     �  	   �     �     �     
            L        l     q     y     �     �     �     �     �     �     �  	   �     �     �     �       
             <	     C	     ]	     w	  ^   �	  e   �	     I
     g
     �
     �
     �
     �
     �
     �
          9  !   N  !   p     �  �   �     �     �  
   �     �     �     �     �       
   #     .  x   K     �     �  )   �          "     7     H  
   [     f  %   u  %   �     �  &   �  9        A     Z                              '                              
                         &   $   /                    *      +      ,           0                 "      (         	         #   .          %      !      -                 )       Actions Add Child Add Parent Address Are you sure you want to delete this child: {{ child.name }}? Are you sure you want to delete this parent: {{ parent.name }}? Assign Role Birth Date Cancel Change Password Children List Contact Number Delete Delete Child Delete Parent Edit Edit Child Edit Parent Email Explore our platform to manage lists of children, their groups, parent information, and much more. First Name Form Title Group Home Last Name List of Children List of Parents Log In Log Out Login My website provides convenient and useful services for parents and children. Name Parents Placeholder Image Register Registration Save Submit Unauthorized Update Update User User List Username Website Image Welcome to My Website Yes, delete Your Image Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n % 1 == 0 && n % 10 == 1 && n % 100 != 11 ? 0 : n % 1 == 0 && n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14) ? 1 : n % 1 == 0 && (n % 10 ==0 || (n % 10 >=5 && n % 10 <=9) || (n % 100 >=11 && n % 100 <=14 )) ? 2: 3);
 Дії Додати Дитину Додати Батька Адреса Ви впевнені, що хочете видалити цю дитину: {{ child.name }}? Ви впевнені, що хочете видалити цього батька: {{ parent.name }}? Призначити Роль Дата Народження Скасувати Змінити пароль Список Дітей Контактний Номер Видалити Видалити Дитину Видалити Батька Редагувати Редагувати Дитину Редагувати Батька Електронна Пошта Досліджуйте нашу платформу для управління списками дітей, їх групами, інформацією про батьків та багато іншого. Ім'я Заголовок Форми Група Домашня сторінка Прізвище Список Дітей Список Батьків Увійти Вийти Авторизуватися Мій веб-сайт надає зручні та корисні послуги для батьків та дітей. Ім'я Батьки Зображення-заповнювач Зареєструватися Реєстрація Зберегти Надіслати Гість Оновити Оновити Користувача Список Користувачів Ім'я Користувача Зображення Веб-сайту Ласкаво просимо на мій веб-сайт Так, видалити Ваше Зображення 
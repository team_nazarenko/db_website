from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User, Group
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView

from .forms import ParentForm, ChildForm
from .forms import RegisterForm
from .models import Parent, Child


def manager_required(user):
    return user.groups.filter(name='Manager').exists()


def admin_required(user):
    return user.groups.filter(name='Administrator').exists()
def user_required(user):
    return user.groups.filter(name='User').exists()

class HomeView(TemplateView):
    template_name = 'home.html'


# Читання записів (Read)
class ParentListView(ListView):
    model = Parent
    template_name = 'parent/parent_list.html'
    context_object_name = "parents"

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class ChildListView(ListView):
    model = Child
    template_name = 'child/child_list.html'
    context_object_name = "childs"

    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

# Створення запису (Create)
class ParentCreateView(CreateView):
    model = Parent
    form_class = ParentForm
    success_url = reverse_lazy('parent_list')
    template_name = 'parent/parent_form.html'

    @method_decorator(user_passes_test(manager_required))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class ChildCreateView(CreateView):
    model = Child
    form_class = ChildForm
    success_url = reverse_lazy('child_list')
    template_name = 'child/child_form.html'

    @method_decorator(user_passes_test(manager_required))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


# Оновлення запису (Update)
class ParentUpdateView(UpdateView):
    model = Parent
    form_class = ParentForm
    success_url = reverse_lazy('parent_list')
    template_name = 'parent/parent_form.html'

    @method_decorator(user_passes_test(manager_required))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class ChildUpdateView(UpdateView):
    model = Child
    form_class = ChildForm
    success_url = reverse_lazy('child_list')
    template_name = 'child/child_form.html'

    @method_decorator(user_passes_test(manager_required))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


# Видалення запису (Delete)
class ParentDeleteView(DeleteView):
    model = Parent
    success_url = reverse_lazy('parent_list')
    template_name = 'parent/parent_confirm_delete.html'

    @method_decorator(user_passes_test(manager_required))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class ChildDeleteView(DeleteView):
    model = Child
    success_url = reverse_lazy('child_list')
    template_name = 'child/child_confirm_delete.html'

    @method_decorator(user_passes_test(manager_required))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class UserListView(ListView):
    model = User
    template_name = 'roles/user_list.html'
    context_object_name = 'users'

    @method_decorator(user_passes_test(admin_required))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class UserUpdateView(UpdateView):
    model = User
    fields = ['username', 'email', 'first_name', 'last_name']
    template_name = 'roles/user_form.html'
    success_url = reverse_lazy('user_list')

    @method_decorator(user_passes_test(admin_required))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class RoleAssignView(UpdateView):
    model = User
    fields = ['username', 'groups']
    template_name = 'roles/role_assign_form.html'
    success_url = reverse_lazy('user_list')

    @method_decorator(user_passes_test(admin_required))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            email = form.cleaned_data.get('email')

            if User.objects.filter(username=username).exists() or User.objects.filter(email=email).exists():
                return render(request, 'registration/register.html', {'form': form,
                                                                      'error_message': 'User already exists. Please choose a different username or email.'})

            user = form.save()
            user.refresh_from_db()
            group = Group.objects.get(name='User')
            user.groups.add(group)
            user.save()
            raw_password = form.cleaned_data.get('password1')

            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = RegisterForm()
    return render(request, 'registration/register.html', {'form': form})




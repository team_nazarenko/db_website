from django.apps import AppConfig


class WebpageConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'WebPage'

    def ready(self):
        import WebPage.signals
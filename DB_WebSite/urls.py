from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.contrib.auth.views import LoginView, PasswordChangeView, PasswordChangeDoneView, LogoutView
from django.urls import path, include

from WebPage import views
from WebPage.views import ParentCreateView, ParentUpdateView, ParentDeleteView, ParentListView, RoleAssignView, \
    UserUpdateView, UserListView
from WebPage.views import ChildCreateView, ChildUpdateView, ChildDeleteView, ChildListView
from WebPage.views import HomeView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('i18n/', include('django.conf.urls.i18n'))

]
urlpatterns += i18n_patterns(
    path('', HomeView.as_view(), name='home'),
    path('register/', views.register, name='register'),
    path('accounts/login/', LoginView.as_view(template_name='registration/login.html'), name='login'),
    path('accounts/password_change/',
         PasswordChangeView.as_view(template_name='registration/password_change_form.html'), name='password_change'),
    path('accounts/password_change/done/',
         PasswordChangeDoneView.as_view(template_name='registration/password_change_done.html'),
         name='password_change_done'),
    path('accounts/logout/', LogoutView.as_view(next_page='login'), name='logout'),
    path('users/', UserListView.as_view(), name='user_list'),
    path('users/<int:pk>/update/', UserUpdateView.as_view(), name='user_form'),
    path('users/<int:pk>/assign_role/', RoleAssignView.as_view(), name='role_assign_form'),

    path('parents/', ParentListView.as_view(), name='parent_list'),
    path('parents/create/', ParentCreateView.as_view(), name='parent_create'),
    path('parents/<int:pk>/update/', ParentUpdateView.as_view(), name='parent_update'),
    path('parents/<int:pk>/delete/', ParentDeleteView.as_view(), name='parent_delete'),

    path('children/', ChildListView.as_view(), name='child_list'),
    path('children/create/', ChildCreateView.as_view(), name='child_create'),
    path('children/<int:pk>/update/', ChildUpdateView.as_view(), name='child_update'),
    path('children/<int:pk>/delete/', ChildDeleteView.as_view(), name='child_delete'),

)
